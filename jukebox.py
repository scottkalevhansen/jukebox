#!/usr/bin/env python3

# -*- coding: utf-8 -*-
from pickle import load

from os import devnull, chmod
from pathlib import Path
import re
import RPi.GPIO as gpio
from random import shuffle
from subprocess import call, Popen, STDOUT
from sys import argv
from threading import Thread
from time import time, sleep

from Jukebox import Jukebox
from Radio import Radio

Path('/tmp/audiostatus').touch()
chmod('/tmp/audiostatus',0o666)

DEVNULL = open(devnull,'w')

gpio.setmode(gpio.BCM)
gpio.setup(21, gpio.IN, pull_up_down=gpio.PUD_UP)

def event_loop():
	global read_pipe, term_flag, jb_path, r_path

	audio_sources = {'jukebox':Jukebox(jb_path),'radio':Radio(r_path)} 
	source_type = 'jukebox'
	audio_source = audio_sources[source_type]

	def pin_change(pin, old_state=[1]):
		new_state = gpio.input(pin)
		change = new_state - old_state[0]
		old_state[0] = new_state
		return change
		
	try:
		last_down_time = None
		last_up_time = None
		last_dah_time = None
		while True:
			sleep(0.01)
			if term_flag:
				audio_source.stop()
				break
			if audio_source.is_finished() and not audio_source.stopped:
				audio_source.next_track()
                
            #CHECK FOR AUDIOCTLPIPE ACTIVITY
			try:        
				with open('/tmp/audioctrl','r+') as f:
					response = f.read()
					f.truncate(0)
				if response != '' and response != None:
					if 'jukebox' in response:
						if source_type != 'jukebox':
							audio_source.stop()
							source_type = response
							audio_source = audio_sources[source_type]
						audio_source.next_track()
					elif 'station' in response:
						if source_type != 'radio':
								audio_source.stop()
								source_type = response
								audio_source = audio_sources['radio']
						name = response.split(' ', 1)[1]
						audio_source.next_track(name=name)
					elif response == 'next':
						audio_source.next_track()
					elif response == 'stop':
						audio_source.stop()
			except OSError:
				pass
			except Exception as e:
				print(e)
	   
            #CHECK FOR SWITCH ACTIVITY
			if pin_change(21) < 0:
				switch_state = 0
				trigger_time = time()
				last_down_time = trigger_time
				while True:
					new_switch_state = gpio.input(21)
                    
					if switch_state < new_switch_state: #switch goes up
						last_up_time = time()
					elif switch_state > new_switch_state: #switch goes down again
						last_down_time = time()
					switch_state = new_switch_state
                    
					if switch_state and last_up_time - last_down_time > 0.05:
						break
         
				if last_up_time - trigger_time > 1:
					audio_source.stop()			#dah
					last_dah_time = last_up_time
				else:
					if last_dah_time is not None and trigger_time - last_dah_time < 0.5:
						audio_source.stop()
						if source_type == 'jukebox':
							source_type = 'radio'
						else:
							source_type = 'jukebox'
						audio_source = audio_sources[source_type]
					audio_source.next_track()   #dit
	finally:
		gpio.cleanup() 

if __name__ == '__main__':
	try:
		path = argv[1]
	except:
		path = '/home/pi/jukebox/'
	jb_path = path + "songDB.pickle"
	r_path = path + "radio.txt"
	
	call(['mpg123', path + 'startup.mp3'], stdout=DEVNULL, stderr=STDOUT)
	term_flag = False
    
	uithread = Thread(target=event_loop)
	uithread.start()
    
	uithread.join()
