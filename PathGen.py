from random import shuffle
class PathGen:

	def __init__(self, paths, randomize=True):
		self.paths = paths
		self.len = len(paths)
		self.randomize = randomize
		if self.randomize:
			shuffle(self.paths)
		self.idx = -1

	def adjust_path(self, path):
		try:
			self.idx = self.paths.index(path)
		except:
			pass

	def next_path(self):
		if self.idx < self.len-1:
			self.idx += 1
			return self.paths[self.idx]
		else:
			last_entry = self.paths[-1] 
			self.idx = 0
			if self.randomize:
				shuffle(self.paths)
				if self.paths[0] == last_entry:
					self.paths[0], self.paths[-1] = self.paths[-1], self.paths[0]
			return self.paths[0]
