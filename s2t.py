#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sys import argv

filename = argv[1]
spaces_per_tab = int(argv[2])

with open(filename, 'r') as f:
	orig_lines = f.readlines()

new_lines = list()

for idx, line in enumerate(orig_lines):
	ls_line = line.lstrip()
	
	if len(line.strip()) > 0:
		num_lead_spaces = len(line) - len(ls_line)
		num_tabs = int(num_lead_spaces / spaces_per_tab)
		new_line = '\t'*num_tabs + ls_line
		if not ls_line[0] in ['#','\''] and num_lead_spaces % spaces_per_tab != 0:
			print(line)
			raise Exception('Unexpected distribution of tabs and spaces')
		new_lines.append(new_line)
	else:
		new_lines.append('')

with open (filename+'.s2t','w') as f:
	f.writelines(new_lines)	
