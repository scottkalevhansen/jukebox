#!/usr/bin/env python3

JUKEBOXDNAME = '<Local Jukebox>'

import curses
import curses.ascii
import re
from sys import argv
from pathlib import Path
from os import chmod

try:
	path = argv[1]
except:
	path = '/home/pi/jukebox/'

Path('/tmp/audioctrl').touch()
chmod('/tmp/audioctrl',0o666)

with open(path + 'radio.txt','r') as f:
	raw_stations = f.readlines()

STATIONS = list()
for rs in raw_stations:
	name, _ = [nug.strip() for nug in re.split('[\[\]]',rs) if nug != '']
	STATIONS.append(name)
STATIONS.append(JUKEBOXDNAME)

FIRSTCOL = 1
FIRSTLINE = 1

num_stations = len(STATIONS)
curr_stn = 0

def move_hilite_delta(screen, delta):
	global curr_stn
	next_stn = max(0,curr_stn + delta)
	next_stn = min(num_stations-1, next_stn)
	move_hilite_direct(screen, next_stn)
	
def move_hilite_direct(screen, next_stn):
	global curr_stn
	screen.addstr(FIRSTLINE + curr_stn, FIRSTCOL, STATIONS[curr_stn])
	screen.addstr(FIRSTLINE + next_stn, FIRSTCOL, STATIONS[next_stn], curses.A_REVERSE)
	curr_stn = next_stn

def write_msg(msg):
	with open('/tmp/audioctrl','w') as f:
		f.write(msg)

def main(screen):
	global curr_stn
	delta = 0
	screen.nodelay(1)
	screen.keypad(True)
	screen.clear()
	slw = screen.subwin(curses.LINES-1, min(curses.COLS,40),0,0)
	slw.box()
	slw.addstr(0,5,"RADIO STATIONS")
	for idx, station in enumerate(STATIONS):
		screen.addstr(FIRSTLINE+idx, FIRSTCOL, station)
	move_hilite_delta(screen, delta)
	while(True):
		screen.refresh()
		curses.napms(50)
		c = screen.getch()
		if c == -1:		#no key was returned
			pass
		elif chr(c) == 'q':  #quit
			break
		elif chr(c) == 'p':
			if STATIONS[curr_stn] == JUKEBOXDNAME:
				write_msg("jukebox")
			else:
				write_msg("station " + STATIONS[curr_stn])
		elif chr(c) == 'o':              
			write_msg("stop")
		elif chr(c) == 'n':
			write_msg("next")
		else:
			if chr(c) == 'w':
				delta = -1
			if chr(c) == 's':
				delta = 1
			if chr(c) == 'd':
				delta = num_stations
			if chr(c) == 'a':
				delta = -num_stations
			move_hilite_delta(screen, delta)
		#CHECK FOR AUDIOSTATUS ACTIVITY
		try:        
			with open('/tmp/audiostatus','r+') as f:
				response = f.read()
				f.truncate(0)
			if response != '' and response != None:
				if 'stopped' in response:
					screen.addstr(curses.LINES-1, 0, "STOPPED")
					screen.clrtoeol()
				elif 'radio' or 'jukebox' in response:
					station = response.split(' ', 1)[1]
					screen.addstr(curses.LINES-1, 0, "NOW PLAYING: " + station)
					screen.clrtoeol()
					if 'radio' in response:
						next_stn = STATIONS.index(station)
					else:
						next_stn = STATIONS.index(JUKEBOXDNAME)
					move_hilite_direct(screen, next_stn)
		except Exception as e:
			print(e)

curses.wrapper(main)
