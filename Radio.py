#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Jukebox import Jukebox
from PathGen import PathGen
from os import devnull
from re import split
from subprocess import call, Popen, STDOUT

DEVNULL = open(devnull,'w')

class Radio(Jukebox):

	def __init__(self, r_path):
		self.player = None
		
		with open(r_path,'r') as f:
			raw_stations = f.readlines()
		self.stationdb = dict()
		for rs in raw_stations:
			name, url = [nug.strip() for nug in split('[\[\]]',rs) if nug != '']
			self.stationdb[name] = url
		
		self.stn_gen = PathGen(list(self.stationdb.keys()), randomize=False)
		self.stopped = True
        
	def next_track(self, name=None):
		self.stop()
		self.stopped = False
		if name is None:
			name = self.stn_gen.next_path()
		else:
			self.stn_gen.adjust_path(name)
		url = self.stationdb[name]
		with open('/tmp/audiostatus','w') as f:
			f.write('radio: ' + name)
		self.player = Popen(['mpg123','-@', url], stdout=DEVNULL, stderr=STDOUT)