#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from PathGen import PathGen
from os import devnull
from pickle import load
from subprocess import call, Popen, STDOUT

DEVNULL = open(devnull,'w')

class Jukebox:

	def __init__(self, dbpath):
		self.player = None
		self.song_path_gen = PathGen(load(open(dbpath,"rb")))
		self.stopped = True

	def stop(self):
		self.stopped = True
		try:
			self.player.kill()
		except:
			pass
		with open('/tmp/audiostatus','w') as f:
			f.write('stopped')

	def next_track(self):
		self.stop()
		self.stopped = False
		song_path = self.song_path_gen.next_path()
		self.player = Popen(['mpg123',song_path], stdout=DEVNULL, stderr=STDOUT)
		with open('/tmp/audiostatus','w') as f:
			f.write('jukebox: ' + song_path)

	def is_finished(self):
		try:
			if self.player.poll() is not None:
				return True
			else:
				return False
		except AttributeError:  #not yet initialized; don't want to autostart
			return False
