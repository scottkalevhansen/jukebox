#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from os import listdir
from os.path import isfile, join
from pickle import dump

musicDir = "/home/pi/jukebox/music"
lMasterPathList = []

def recursiveSearch(dir):
	lFiles = listdir(dir)
	while len(lFiles) > 0:
		fileOrDir = join(dir,lFiles.pop())
		if isfile(fileOrDir):
			if fileOrDir[-4:] == ".mp3":
				lMasterPathList.append(fileOrDir)
		else:
			recursiveSearch(fileOrDir)

recursiveSearch(musicDir)

print (lMasterPathList)
dump(lMasterPathList, open("./songDB.pickle","wb"))